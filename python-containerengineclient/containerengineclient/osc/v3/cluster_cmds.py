#   Copyright 2016 Huawei, Inc. All rights reserved.
#   Licensed under the Apache License, Version 2.0 (the "License"); you may
#   not use this file except in compliance with the License. You may obtain
#   a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#   WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#   License for the specific language governing permissions and limitations
#   under the License.
#
import logging

from containerengineclient.common import parser_builder as bpb
from containerengineclient.common.i18n import _
from containerengineclient.osc.v3 import parser_builder as pb
from containerengineclient.v3 import resource
from osc_lib.command import command

LOG = logging.getLogger(__name__)

class ListClusters(command.Lister):
    _description = _("cluster list")

    def get_parser(self, prog_name):
        parser = super(ListClusters, self).get_parser(prog_name)
        #pb.ClusterParser.add_start_arg(parser)
        bpb.BaseParser.add_limit_option(parser, 1000)
        # bpb.BaseParser.add_order_option(parser)
        return parser

    def take_action(self, args):
        cluster_mgr = self.app.client_manager.containerengine.cluster_mgr
        clusters = cluster_mgr.list(limit=args.limit)
        columns = resource.Cluster.list_column_names
        return columns, (m.get_display_data(columns) for m in clusters)

class ShowCluster(command.ShowOne):
    _description = _("cluster show")

    def get_parser(self, prog_name):
        parser = super(ShowCluster, self).get_parser(prog_name)
        pb.ClusterParser.add_cluster_id_arg(parser, 'Show')
        return parser

    def take_action(self, args):
        cluster_mgr = self.app.client_manager.containerengine.cluster_mgr
        cluster = cluster_mgr.show(cluster_id=args.cluster_id)
        columns = resource.Cluster.show_column_names
        return columns, cluster.get_display_data(columns) 
