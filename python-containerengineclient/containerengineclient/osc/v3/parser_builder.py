#!/usr/bin/env python
# -*- coding: utf-8 -*-
#   Licensed under the Apache License, Version 2.0 (the "License"); you may
#   not use this file except in compliance with the License. You may obtain
#   a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#   WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#   License for the specific language governing permissions and limitations
#   under the License.
#
from containerengineclient.common import parsetypes
from containerengineclient.common.i18n import _


class ClusterParser(object):

    @staticmethod
    def add_cluster_id_arg(parser, op):
        parser.add_argument(
            'cluster_id',
            metavar="<cluster-id>",
            help=_("%s cce cluster for id." % op)
        )

    @staticmethod
    def add_start_arg(parser, required=False):
        parser.add_argument(
            "--start",
            required=required,
            metavar="<key=value>",
            help=_("return result list start from ("
                   "namespace.metric-name.key:value)"),
        )

    @staticmethod
    def add_from_arg(parser, required=True):
        parser.add_argument(
            "--from",
            required=required,
            dest="from_",
            metavar="<yyyy-MM-ddTHH:mm:ss>",
            type=parsetypes.date_type('%Y-%m-%dT%H:%M:%S'),
            help=_("metric data after this time "
                   "(UTC timestamp with format yyyy-MM-ddTHH:mm:ss)"),
        )

    @staticmethod
    def add_to_arg(parser, required=True):
        parser.add_argument(
            "--to",
            required=required,
            metavar="<yyyy-MM-ddTHH:mm:ss>",
            type=parsetypes.date_type('%Y-%m-%dT%H:%M:%S'),
            help=_("metric data before this time "
                   "(UTC timestamp with format yyyy-MM-ddTHH:mm:ss)"),
        )

    @staticmethod
    def add_filter_arg(parser, required=True):
        parser.add_argument(
            "--filter",
            required=required,
            choices=["average", "variance", "min", "max"],
            help=_("filter by data aggregation method"),
        )
