#!/usr/bin/env python
# -*- coding: utf-8 -*-
#   Licensed under the Apache License, Version 2.0 (the "License"); you may
#   not use this file except in compliance with the License. You may obtain
#   a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#   WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#   License for the specific language governing permissions and limitations
#   under the License.
#
import time
from containerengineclient.common import manager
from containerengineclient.common import utils
from containerengineclient.v3 import resource


class ClusterManager(manager.Manager):
    """Container Engine Cluster API management"""

    resource_class = resource.Cluster

    def list(self, limit=None):
        """list cluster
 
        :return:
        """
        params = utils.remove_empty_from_dict({
            "limit": limit
        })

        projectUrl = '/api/v3/projects/{project_id}/clusters'.format( project_id=self.http_client.get_project_id() )

       
        return self._list(projectUrl, key='items', params=params)


    def show(self, cluster_id):
        """show cluster details

        :param cluster_id:
        :return:
        """
        params = utils.remove_empty_from_dict({
            "cluster_id": cluster_id
        })

        projectUrl = '/api/v3/projects/{pid}/clusters/{id}'.format( pid=self.http_client.get_project_id(), id=cluster_id )
       
        return self._get(projectUrl, key='', params=params)
