#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Licensed under the Apache License, Version 2.0 (the "License"); you may
#   not use this file except in compliance with the License. You may obtain
#   a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#   WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#   License for the specific language governing permissions and limitations
#   under the License.
#

from containerengineclient.common import display
from containerengineclient.common import resource
from containerengineclient.common import utils


class Cluster(resource.Resource, display.Display):
    """Container engine cluster resource instance."""

    list_column_names = [
        "Name",
        "Id",
        "State",
        "Version", 
        "Type", 
        "Flavor",
        "Created",
        "Updated"
    ]

    @property
    def name(self):
        return self.metadata['name']

    @property
    def id(self):
        return self.metadata['uid']

    @property
    def created(self):
        return self.metadata['creationTimestamp']

    @property
    def updated(self):
        return self.metadata['updateTimestamp']
    
    @property
    def version(self):
        return self.spec['version']
    
    @property
    def type(self):
        return self.spec['type']

    @property
    def flavor(self):
        return self.spec['flavor']

    @property
    def state(self):
        return self.status['phase']

    show_column_names = [
        "Name",
        "Id",
        "Created",
        "Updated",
        "apiVersion",                
        "Version", 
        "Type", 
        "Flavor",
        "HostVpc",
        "HostSubnet",
        "ContainerNetMode",
        "ContainerNetCidr",
        "authMode",
        "billingMode",
        "State",
        "intEndpoint",
        "extEndpoint",
        "extOtcEndpoint"
    ]

    @property
    def apiversion(self):
        return self.apiVersion

    @property
    def hostvpc(self):
        return self.spec['hostNetwork']['vpc']

    @property
    def hostsubnet(self):
        return self.spec['hostNetwork']['subnet']

    @property
    def containernetmode(self):
        return self.spec['containerNetwork']['mode']

    @property
    def containernetcidr(self):
        return self.spec['containerNetwork']['cidr']

    @property
    def authmode(self):
        return self.spec['authentication']['mode']

    @property
    def billingmode(self):
        return self.spec['billingMode']

    @property
    def intendpoint(self):
        return self.status['endpoints']['internal']

    @property
    def extendpoint(self):
        endpoints = self.status['endpoints']
        if 'external' in endpoints:
           return endpoints['external']
        return ''   

    @property
    def extotcendpoint(self):
        endpoints = self.status['endpoints']
        if 'external_otc' in endpoints:
           return endpoints['external_otc']
        return ''   
