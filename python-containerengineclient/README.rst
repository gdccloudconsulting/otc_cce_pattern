python-cloudeyeclient
=====================

This is a `OpenStack Client`_ plugin for Open Telekom Cloud Container engine API which provides **command-line scripts** (integrated with openstack) and Python library for accessing the proprietary Kubernetes Cluster Management functions. It is especially useful to get access certificates from the Container Engine to enable kubectl usage.


Installation
------------

1. Install python-openstackclient
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This project is a plugin of  `OpenStack Client`_. Therefor, you need
to have `OpenStack Client`_ installed before using the plugin. You can
install `OpenStack Client`_ by pip::

    pip install python-openstackclient

*To get more information about python-openstackclient, please check the
official site* `OpenStack Client`_

2. Install plugin
^^^^^^^^^^^^^^^^^^

Currently, We can install the plugin from source code

.. code:: console

    $ git clone https://github.com/Huawei/OpenStackClient_CCE python-cloudcontainerengine
    $ cd python-cloudcontainerengine
    # use python setup.py develop for test purpose
    $ python setup.py install
    $ pip install -r requirements.txt


Command Line Client Usage
-----------------------------------------

.. note::

    The command line client is self-documenting. Use the --help or -h flag to access the usage options.
    You can find more command line client examples `here <./commands.rst>`_


This plugin is integrated with `OpenStack Client`_ , so the command line client
follow all the usage **openstack** provided.

.. code:: console

    $ openstack --help
    usage: openstack [--version] [-v | -q] [--log-file LOG_FILE] [-h] [--debug]
                 [--os-cloud <cloud-config-name>]
                 [--os-region-name <auth-region-name>]
                 [--os-cacert <ca-bundle-file>] [--os-cert <certificate-file>]
                 [--os-key <key-file>] [--verify | --insecure]
                 [--os-default-domain <auth-domain>]
                 [--os-interface <interface>] [--timing] [--os-beta-command]
                 [--os-profile hmac-key]
                 [--os-compute-api-version <compute-api-version>]
                 [--os-network-api-version <network-api-version>]
                 [--os-image-api-version <image-api-version>]
                 [--os-volume-api-version <volume-api-version>]
                 [--os-identity-api-version <identity-api-version>]
                 [--os-object-api-version <object-api-version>]
                 [--os-queues-api-version <queues-api-version>]
                 [--os-clustering-api-version <clustering-api-version>]
                 [--os-search-api-version <search-api-version>]
                 .......


Cloud-Container-Engine-Service Client contains commands list in table below, use -h option to get more useage

+----------------------+
| command              |
+======================+
| cluster list         |
+----------------------+
| cluster show         |
+----------------------+
| cluster cert show    |
+----------------------+

.. code:: console

    # use help command 
    $ openstack cluster list -h

.. code:: console

    # list clusters 
    $ openstack cluster list

Python Library Usage
-------------------------------

Here's an example of listing metric types using Python library with keystone V3 authentication:

.. code:: python

    >>> from keystoneauth1 import session
    >>> from keystoneauth1 import identity
    >>> from containerengineclient.v3 import client

    >>> # Use Keystone API v3 for authentication as example
    >>> auth = identity.v3.Password(auth_url=u'http://localhost:5000/v3',
    ...                             username=u'admin_user',
    ...                             user_domain_name=u'Default',
    ...                             password=u'password',
    ...                             project_name=u'demo',
    ...                             project_domain_name=u'Default')

    >>> # Next create a Keystone session using the auth plugin we just created
    >>> session = session.Session(auth=auth)

    >>> # Now we use the session to create a CloudEye client
    >>> client = client.Client(session=session)

    >>> # Then we can access all Cloud Eye API
    >>> # Let's try list metric API
    >>> client.metric_mgr.list(namespace='SYS.VPC')
    [<Metric namespace=SYS.VPC ....>, ....]


.. note::

    The example above must be running and configured to use the Keystone Middleware.

    For more information on setting this up please visit: `KeyStone`_


* License: Apache License, Version 2.0
* `OpenStack Client`_
* `CloudEye Offical Document`_
* `KeyStone`_

.. _OpenStack Client: https://github.com/openstack/python-openstackclient
.. _CloudEye Offical Document: http://support.hwclouds.com/ces/
.. _KeyStone: http://docs.openstack.org/developer/keystoneauth/
