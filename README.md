# README #

This repo contains typical usage pattern for the Cloud Container Engine (CCE) of the Open Telekom Cloud (OTC).

### What does the repo contain? ###

###### A setup description for the pure Kubernetes kubectl and dashboard for OTC
  
Go to [cce-master](https://bitbucket.org/gdccloudconsulting/otc_cce_pattern/src/a8cc59c7870a8f5dd618a35bcb0ff221b939ed62/cce-master/README.MD?at=master&fileviewer=file-view-default)

###### A small demo for a basic microservice deployment with ELK (ElasticSearch-Logstash-Kibana)  

Go to [elk-obs](https://bitbucket.org/gdccloudconsulting/otc_cce_pattern/src/700b12692f64e6c006b8b1c5450f81b81df05dd0/elk-obs/?at=master)
  
