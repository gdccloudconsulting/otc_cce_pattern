#!/bin/bash

# Copyright 2014 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

# This is a modification of the orginal install script for GKE
# to install openshift on existing OTC CCE
# See
# https://github.com/kubernetes/examples/blob/master/staging/openshift-origin/README.md
# for the original installation example

# Creates resources from the example, assumed to be run from Kubernetes repo root
echo
echo "===> Initializing:"
if [ ! $(which python) ]
then
	echo "Python is a prerequisite for running this script. Please install Python and try running again."
	exit 1
fi

if [ ! $(which otc) ]
then
	echo "otc.sh helper tool is a prerequisite for running this script. Please install otc.sh and try running again."
	exit 1
fi

CLUSTERNAME=$1

export OPENSHIFT_EXAMPLE=$(pwd)
echo Set OPENSHIFT_EXAMPLE=${OPENSHIFT_EXAMPLE}
export OPENSHIFT_CONFIG=${OPENSHIFT_EXAMPLE}/config
echo Set OPENSHIFT_CONFIG=${OPENSHIFT_CONFIG}
mkdir ${OPENSHIFT_CONFIG}
echo Made dir ${OPENSHIFT_CONFIG}
echo

echo "===> Setting up OpenShift-Origin namespace:"
kubectl create -f ${OPENSHIFT_EXAMPLE}/openshift-origin-namespace.yaml
echo

echo "===> Setting up etcd-discovery:"
# A token etcd uses to generate unique cluster ID and member ID. Conforms to [a-z0-9]{40}
export ETCD_INITIAL_CLUSTER_TOKEN=$(python -c "import string; import random; print(''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(40)))")

# A unique token used by the discovery service. Conforms to etcd-cluster-[a-z0-9]{5}
export ETCD_DISCOVERY_TOKEN=$(python -c "import string; import random; print(\"etcd-cluster-\" + ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(5)))")
sed -i.bak -e "s/INSERT_ETCD_INITIAL_CLUSTER_TOKEN/\"${ETCD_INITIAL_CLUSTER_TOKEN}\"/g" -e "s/INSERT_ETCD_DISCOVERY_TOKEN/\"${ETCD_DISCOVERY_TOKEN}\"/g" ${OPENSHIFT_EXAMPLE}/etcd-controller.yaml

kubectl create -f ${OPENSHIFT_EXAMPLE}/etcd-discovery-controller.yaml --namespace='openshift-origin'
kubectl create -f ${OPENSHIFT_EXAMPLE}/etcd-discovery-service.yaml --namespace='openshift-origin'
echo

echo "===> Setting up etcd:"
kubectl create -f ${OPENSHIFT_EXAMPLE}/etcd-controller.yaml --namespace='openshift-origin'
kubectl create -f ${OPENSHIFT_EXAMPLE}/etcd-service.yaml --namespace='openshift-origin'
echo

# on OTC CCE, it is required to install the deployment first
# and the loadbalancer with the service second
# ENVIRONMENT PUBLIC_OPENSHIFT_IP must be set



echo "===> Setting up openshift-origin:"

echo "===> Configuring OpenShift:"
kubectl config view --output=yaml --flatten=true --minify=true > ${OPENSHIFT_CONFIG}/kubeconfig

echo "===> Deriving Openshift secrets from Kubernetes:"
docker run --privileged -v ${OPENSHIFT_CONFIG}:/config openshift/origin start master --write-config=/config --kubeconfig=/config/kubeconfig --master=https://10.99.19.158:5443 --etcd=http://etcd:2379
sudo -E chown -R ${USER} ${OPENSHIFT_CONFIG}

export MASTER_INSTANCE=$(otc ecs list name=${CLUSTERNAME}"-node-1" | awk '{print $6}' | head -1)
echo "sudo cat /var/paas/srv/kubernetes/kubecfg.key; exit;" | ssh -q -l linux ${MASTER_INSTANCE} | grep -Ex "(^\-.*\-$|^\S+$)" > ${OPENSHIFT_CONFIG}/serviceaccounts.private.key
echo "sudo cat /var/paas/srv/kubernetes/kubecfg.crt; exit;" | ssh -q -l linux ${MASTER_INSTANCE} | grep -Ex "(^\-.*\-$|^\S+$)" > ${OPENSHIFT_CONFIG}/serviceaccounts.public.crt
echo "sudo cat /var/paas/srv/kubernetes/ca.crt; exit;" | ssh -q -l linux ${MASTER_INSTANCE} | grep -Ex "(^\-.*\-$|^\S+$)" > ${OPENSHIFT_CONFIG}/ca.crt
# The following insertion will fail if indentation changes
sed -i -e 's/publicKeyFiles:.*$/publicKeyFiles:/g' -e '/publicKeyFiles:/a \ \ - serviceaccounts.private.key' ${OPENSHIFT_CONFIG}/master-config.yaml
kubectl create secret generic openshift-config --from-file=${OPENSHIFT_CONFIG} --namespace="openshift-origin"
echo

echo "===> Running OpenShift Master:"
kubectl create -f ${OPENSHIFT_EXAMPLE}/openshift-controller.yaml --namespace='openshift-origin'

echo "===> Opening OTC external loadbalancer"
kubectl create -f ${OPENSHIFT_EXAMPLE}/openshift-service.yaml --namespace='openshift-origin'
echo

#export PUBLIC_OPENSHIFT_IP=""
#echo "===> Waiting for public IP to be set for the OpenShift Service."
#echo "Mistakes in service setup can cause this to loop infinitely if an"
#echo "external IP is never set. Ensure that the OpenShift service"
#echo "is set to use an external load balancer. This process may take" 
#echo "a few minutes. Errors can be found in the log file found at:"
#echo ${OPENSHIFT_EXAMPLE}/openshift-startup.log
#echo "" > ${OPENSHIFT_EXAMPLE}/openshift-startup.log
#while [ ${#PUBLIC_OPENSHIFT_IP} -lt 1 ]; do
#	echo -n .
#	sleep 1
#	{
#		export PUBLIC_OPENSHIFT_IP=$(kubectl get services openshift --namespace="openshift-origin" --template="{{ index .status.loadBalancer.ingress 0 \"ip\" }}")
#	} >> ${OPENSHIFT_EXAMPLE}/openshift-startup.log 2>&1
#	if [[ ! ${PUBLIC_OPENSHIFT_IP} =~ ^([0-9]{1,3}\.){3}[0-9]{1,3}$ ]]; then
#		export PUBLIC_OPENSHIFT_IP=""
#	fi
#done
#echo
#echo "Public OpenShift IP set to: ${PUBLIC_OPENSHIFT_IP}"
#echo

echo Done.
